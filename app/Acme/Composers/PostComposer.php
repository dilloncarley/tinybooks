<?php namespace Acme\Composers;

use DB, Auth;

class PostComposer 
{

    /**
     * Compose a post
     *
     * @param $view
     */
    public function compose($view)
    {

        if (Auth::check())
        {

      
        $favorites = DB::table('favorites')->whereUserId(Auth::user()->id)->lists('book_id');


          
 $counts = DB::select('SELECT (post_id) as post, count(post_id) AS count
    
    FROM favorites
    GROUP BY post_id
    HAVING count > 1');
    




  
        $view->with('favorites', $favorites)->with('counts', $counts);



        }
    }
}