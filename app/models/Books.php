<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Books extends Eloquent implements UserInterface, RemindableInterface {
	
	public $timestamps = true;


     public $errors;
    protected $guarded =['id', 'created_at', 'updated_at'];

	protected $fillable = ['title','coverurl', 'content','author','published'];

	public static $rules =[
		'title' => 'required',
		'coverurl'		=> 'required',
		'content' => 'required'
		


	];
 
	public  function  isValid($data)
	{
		$validation = Validator::make($data, static::$rules);


		if ($validation->passes()) return true;

		$this->errors = $validation->messages();
		return false;
		
	}
	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'books';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
	

}
