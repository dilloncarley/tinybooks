<?php

Route::get('/explore', 'BooksController@index');
Route::get('/drafts', 'BooksController@drafts');
Route::get('books/{book}/edit', 'BooksController@edit');

Route::get('login','SessionsController@create');


Route::get('logout','SessionsController@destroy');
Route::get('/', function()
{
   return View::make('sessions.login');
});




Route::resource('sessions', 'SessionsController');





Route::resource('books/{book}/edit', 'BooksController@edit');

Route::delete('books/{book}/destroy', 'BooksController@destroy');
Route::resource('write', 'BooksController@create');


Route::resource('drafts', 'BooksController@drafts');


Route::resource('users','UsersController');



Route::resource('books', 'BooksController');



Route::group(array('before' => 'auth'), function()
{
   Route::get('users', array('as' => 'users', 'uses' => 'UsersController@index'));
    Route::get('books', array('as' => 'books', 'uses' => 'BooksController@index'));
    Route::get('write', array('as' => 'write', 'uses' => 'BooksController@create'));
});

//favorites




		 

Route::delete('favorites/{booksId}', ['as' => 'favorites.destroy', function($booksId)
{

	Auth::user()->favorites()->detach($booksId);
return Redirect::back();

}])->before('auth|csrf');

Route::get('users/{id}/favorites', function ($userId)
{


$posts = User::findOrFail($userId)->favorites;
return View::make('books.favorites', compact('posts'));


});


//Options: create a new setting
Route::post( 'favorites', array(
    'as' => 'favorites.store',
    'uses' => 'FavoritesController@create'
) );

Route::post(
    'books/search', 
    array(
        'as' => 'books.search', 
        'uses' => 'BooksController@booksSearch'
    )
);