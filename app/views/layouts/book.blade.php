<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no"> 
		<title></title>
		<meta name="description" content="Fullscreen Pageflip Layout with BookBlock" />
		<meta name="keywords" content="fullscreen pageflip, booklet, layout, bookblock, jquery plugin, flipboard layout, sidebar menu" />
		<meta name="author" content="Codrops" />
		<link rel="shortcut icon" href="../favicon.ico"> 
	 	{{HTML::style('css/jquery.jscrollpane.custom.css')}}
		{{HTML::style('css/bookblock.css')}}
		{{HTML::style('css/custom.css')}}
		{{HTML::style('css/animate.css')}}
		{{HTML::style('http://fonts.googleapis.com/css?family=EB+Garamond')}}
		{{HTML::style('fonts/billabong.otf')}}

<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">

{{ HTML::script('js/modernizr.custom.79639.js') }}
		
	</head>
<body  id="body"  onload="setTimeout(function() { window.scrollTo(0, 1) }, 100);" style="background:url('http://tinybooks.mobi/images/bg3.png');">
<div class="top-menubar" style="background:url('http://tinybooks.mobi/images/bg3.png');z-index: 1000;bottom:0;">
		    <center><div id="logocontainer" > <img  id="logo" src="{{ URL::asset('images/logo.png') }}" /></div></center>
	

		<span id="tblcontents" class="menu-button">Table of Contents</span>

<br>
<br>
<br>
</div>

<div id="container" class="container">	


<div class="menu-panel">

<h3>@if (Auth::check())
				Logged in as <br>{{ Auth::user()->username }}
{{ HTML::link('/logout', 'log out')}}
   
@endif</h3>
		
			
				<ul  class="menu">
					<li id="dashboard" class="menu-toc-current"><a href="/users">Dashboard</a></li>
					<li id="explore"><a href="/books">Explore</a></li>
					<li id="write"> <a href="/write">Write</a></li>
					<li id="account" ><a href="/users/{{Auth::user()->username}}">Account</a></li>
					<li id="drafts" ><a  href="/drafts">Drafts</a></li>
					<li id="bug"><a  href="/users"> Report a Bug</a></li>
				</ul>
			
				
			</div>



<div class="animsition">
			<div class="bb-custom-wrapper">
				<div id="bb-bookblock" class="bb-bookblock">
@yield('content')
				</div>
				</div>
				</div>
	
	


{{ HTML::script('http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js') }}
{{ HTML::script('js/jquery.mousewheel.js') }}
{{ HTML::script('js/jquery.jscrollpane.min.js') }}
{{ HTML::script('js/jquerypp.custom.js') }}
{{ HTML::script('js/jquery.bookblock.js') }}
{{ HTML::script('js/page.js') }}
{{ HTML::script('js/animsition.js') }}
{{HTML::style('css/animsition.min.css')}}
{{ HTML::script('js/jquery.onHold.js') }}
{{ HTML::script('js/eldarion-ajax.min.js') }}




<script>


 $(".pagecount").each(function(i) {
 $(this).find('span').text(++i);


  
});



</script>

<script>
			$(function() {

				Page.init();

			});
		</script>

		<script>
		$(document).ready(function(){
		$('#info').click(function() { 
    document.location = '/books/{$books->id}/edit") ';
} );
		})
		</script>
<script>

$(document).ready(function(){


    $('.content').onHold(function(e){
        e.preventDefault();
        var test = $('.test');
        if(test.hasClass('showed')) {  
            test.removeClass('showed').addClass('animated bounceOutRight');
        }else{
            test.addClass('showed').removeClass('animated bounceOutRight').addClass('animated bounceInRight');
        }
    })
    	
})
		</script>

<script type="text/javascript">
$(document).ready(function() {
  
  $(".animsition").animsition({
  
    inClass               :   'fade-in-right',
    outClass              :   'fade-out-left',
    inDuration            :    200,
    outDuration           :    200,
    linkElement           :   'a', 
    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
    loading               :    true,
    loadingParentElement  :   'body', //animsition wrapper element
    loadingClass          :   'animsition-loading',
    unSupportCss          : [ 'animation-duration',
                              '-webkit-animation-duration',
                              '-o-animation-duration'
                            ],
    //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser. 
    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
    
    overlay               :   false,
    
    overlayClass          :   'animsition-overlay-slide',
    overlayParentElement  :   'body'
  });
});
</script>


</body>
</html>
