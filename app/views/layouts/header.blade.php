
{{HTML::style('css/jquery.jscrollpane.custom.css')}}
		{{HTML::style('css/bookblock.css')}}
		{{HTML::style('css/custom.css')}}
		{{HTML::style('css/animate.css')}}
		{{HTML::style('css/animsition.min.css')}}
		{{HTML::style('http://fonts.googleapis.com/css?family=EB+Garamond')}}
		{{HTML::style('fonts/billabong.otf')}}
<div class="top-menubar" style="background:#3399ff;z-index: 1000;bottom:0;">
		    <center><div id="logocontainer" > <img  id="logo" src="{{ URL::asset('images/logoo.png') }}" /></div></center>
	

		<span id="tblcontents" class="menu-button">Table of Contents</span>

<br>
<br>
<br>
</div>


<div id="container" class="container">	

	
<div class="menu-panel">

				<h3>@if (Auth::check())
				Logged in as <br>{{ Auth::user()->username }}
{{ HTML::link('/logout', 'log out')}}
   
@endif</h3>
			
				<ul  class="menu">
					<li id="dashboard" class="menu-toc-current"><a href="/users">Dashboard</a></li>
					<li id="explore"><a href="/books">Explore</a></li>
					<li id="write"> <a href="/write">Write</a></li>
					<li id="account" ><a href="/users/{{Auth::user()->username}}">Account</a></li>
					<li id="drafts" ><a  href="/drafts">Drafts</a></li>
					<li id="bug"><a  href="/users"> Report a Bug</a></li>
				
				</ul>
			
				
			</div>

			@yield('content')