<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
		<title></title>
		<meta name="description" content="Fullscreen Pageflip Layout with BookBlock" />
		<meta name="keywords" content="fullscreen pageflip, booklet, layout, bookblock, jquery plugin, flipboard layout, sidebar menu" />
		<meta name="author" content="Codrops" />
		<link rel="shortcut icon" href="../favicon.ico"> 
	 	{{HTML::style('css/jquery.jscrollpane.custom.css')}}
		{{HTML::style('css/bookblock.css')}}
		{{HTML::style('css/custom.css')}}

		{{HTML::style('css/animate.css')}}
		{{ HTML::script('js/modernizr.custom.79639.js') }}
		{{HTML::style('fonts/billabong.otf')}}
	

		
	</head>
<body  id="body"  onload="setTimeout(function() { window.scrollTo(0, 1) }, 100);" >
<div class="top-menubar" style="background:white;">
		    <center><div id="logocontainer" > <img  id="logo" src="{{ URL::asset('images/logo.png') }}" /></div></center>
	

		<span id="tblcontents" class="menu-button">Table of Contents</span>

<br>
<br>
<br>
</div>


<div id="container" class="container">	

	
<div class="menu-panel">
				
			
				<ul  class="menu">
					<li class="menu-toc-current"><a href="/users">Users</a></li>
					<li><a href="/books">books</a></li>
					
				</ul>
			
				
			</div>




			<div class="bb-custom-wrapper">
				
@yield('content')
			
				</div>
				</div>
	
	


{{ HTML::script('http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js') }}
{{ HTML::script('js/jquery.mousewheel.js') }}
{{ HTML::script('js/jquery.jscrollpane.min.js') }}
{{ HTML::script('js/jquerypp.custom.js') }}
{{ HTML::script('js/jquery.bookblock.js') }}
{{ HTML::script('js/page.js') }}



<script>
			$(function() {

				Page.init();

			});
		</script>

		<script>
		$(document).ready(function(){
		$('#info').click(function() { 
    document.location = '/books/{$books->id}/edit") ';
} );
		})
		</script>
<script>

$(document).ready(function(){


    $('.toggle').click(function(e){
        e.preventDefault();
        var test = $('.test');
        if(test.hasClass('showed')) {  
            test.removeClass('showed').addClass('animated bounceOutRight');
        }else{
            test.addClass('showed').removeClass('animated bounceOutRight').addClass('animated bounceInRight');
        }
    })
    	
})
		</script>
<script>
$(document).ready(function(){
    // to fade in on page load
    $("#body").css("display", "none");
    $("#body").fadeIn(400); 
    // to fade out before redirect
    $('a').click(function(e){
        redirect = $(this).attr('href');
        e.preventDefault();
        $('#body').fadeOut(400, function(){
            document.location.href = redirect
        });
    });
})
</script>



</body>
</html>
