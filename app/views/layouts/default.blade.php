<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no"> 
		
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<meta name="author" content="" />
		<link rel="shortcut icon" href="../favicon.ico"> 
	 	{{HTML::style('css/jquery.jscrollpane.custom.css')}}
		{{HTML::style('css/bookblock.css')}}
		{{HTML::style('css/custom.css')}}
		{{HTML::style('css/animate.css')}}
		{{HTML::style('css/animsition.min.css')}}
		{{HTML::style('http://fonts.googleapis.com/css?family=EB+Garamond')}}
		{{HTML::style('fonts/billabong.otf')}}
	 
{{ HTML::script('js/modernizr.custom.79639.js') }}
		

	</head>
	  

<body  id="body"  >
<div class="top-menubar" style="background:url('http://tinybooks.mobi/images/bg3.png');z-index: 1000;bottom:0;">
		    <div id="logocontainer" > <center><img  id="logo" src="{{ URL::asset('images/logo.png') }}" /></center></div>
	

		<span id="tblcontents" class="menu-button">Table of Contents</span>

<br>
<br>
<br>
</div>


<div id="container" class="container">	

	
<div class="menu-panel">

				<h3>@if (Auth::check())
				Logged in as <br>{{ Auth::user()->username }}
{{ HTML::link('/logout', 'log out')}}
   
@endif</h3>
			
				<ul  class="menu">

				<div class="search">
	{{ Form::model(null, array('route' => array('books.search'))) }}
	{{ Form::text('query', null, array( 'placeholder' => 'Search query...' )) }}
	{{ Form::submit('Search') }}
	{{ Form::close() }}
</div>
					<li id="dashboard" class="menu-toc-current"><a href="/users">Dashboard</a></li>
					<li id="explore"><a href="/books">Explore</a></li>
					<li id="write"> <a href="/write">Write</a></li>
					<li id="account" ><a href="/users/{{Auth::user()->username}}">Account</a></li>
					<li id="drafts" ><a  href="/drafts">Drafts</a></li>
					<li id="bug"><a  href="/users"> Report a Bug</a></li>
				
				</ul>
			
				
			</div>


<div class="animsition">

			<div class="bb-custom-wrapper">
				<center>
@yield('content')
			</center>
				</div>
				</div>
				</div>
	


{{ HTML::script('http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js') }}
{{ HTML::script('js/jquery.mousewheel.js') }}
{{ HTML::script('js/jquery.jscrollpane.min.js') }}
{{ HTML::script('js/jquerypp.custom.js') }}
{{ HTML::script('js/jquery.bookblock.js') }}
{{ HTML::script('js/page.js') }}
{{ HTML::script('js/animsition.js') }}
{{ HTML::script('js/jquery.onHold.js') }}

{{ HTML::script('http://thecodeplayer.com/uploads/js/jquery.easing.min.js') }}


   
<script>
$(document).ready(function(){
 $(".delete").click(function(event) {
        if( !confirm('Are you sure you want to delete this book?') ) 
            event.preventDefault();
    });

})
</script>
	

<script>
			$(function() {

				Page.init();

			});
		</script>

		<script>
		$(document).ready(function(){
		$('#info').click(function() { 
    document.location = '/books/{$books->id}/edit") ';
} );
		})
		</script>
<script>

$(document).ready(function(){


    $('.toggle').click(function(e){
        e.preventDefault();
        var test = $('.test');
        if(test.hasClass('showed')) {  
            test.removeClass('showed').addClass('animated bounceOutRight');
        }else{
            test.addClass('showed').removeClass('animated bounceOutRight').addClass('animated bounceInRight');
        }
    })
    	
})
		</script>

<script type="text/javascript">
$(document).ready(function() {
  
  $(".animsition").animsition({
  
    inClass               :   'fade-in-right',
    outClass              :   'fade-out-left',
    inDuration            :    200,
    outDuration           :    200,
    linkElement           :   'a', 
    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
    loading               :    true,
    loadingParentElement  :   'body', //animsition wrapper element
    loadingClass          :   'animsition-loading',
    unSupportCss          : [ 'animation-duration',
                              '-webkit-animation-duration',
                              '-o-animation-duration'
                            ],
    //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser. 
    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
    
    overlay               :   false,
    
    overlayClass          :   'animsition-overlay-slide',
    overlayParentElement  :   'body'
  });
});
</script>


</body>
</html>