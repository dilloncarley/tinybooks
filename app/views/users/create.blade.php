@extends('layouts.default')

@section('content')
	<h1> Create New user </h1>


	{{ Form::open(['route' => 'users.store']) }}
       <div>
			{{ Form::label('username', 'Username') }}

			{{ Form::input('text','username') }}

			{{$errors->first('username') }}
	</div>

  <div>
			{{ Form::label('email', 'Email') }}

			{{ Form::email('email')}}

			{{$errors->first('email') }}
	</div>

	<div>
		{{ Form::label('password', 'Password') }}

		{{ Form::password('password') }}

		{{$errors->first('password') }}
	</div>
	<div>
		{{ Form::label('password_confirmation', 'Password') }}

		{{ Form::password('password_confirmation') }}

		
	</div>

	<div> {{ Form::submit('Create user') }}</div>

	{{ Form::close() }}


	@stop	
