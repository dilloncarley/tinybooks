@extends('layouts.form')

@section('content')

	{{ Form::open(['route' => 'books.store',  'method' =>'post'] ) }}

					 <div>

								{{ Form::label('title', 'Title') }}

								{{ Form::input('text','title') }}

								{{$errors->first('title') }}
																	


																	</div>

					<div>

								{{ Form::label('coverurl', 'Book Cover url') }}

								{{ Form::input('text', 'coverurl') }}

								{{$errors->first('coverurl') }}
														

																	</div>
	


						<div >
						   
						   
						    
								{{ Form::label('content', 'Content')  }}

								
								{{ Form::textarea('content', 'content', array('id' => 'content')) }}
								

								{{ $errors->first('content') }}

																	</div>

								
								
						<div>
							 

							 {{ Form::hidden('author', $value) }} 
							 {{ Form::hidden('published', '0') }} 

							                                       </div>

						<input type="submit" class="action-button" name="Post" value="Publish">
						<input type="submit" class="action-button" name="draft" value="Draft">

	{{ Form::close() }}


	@stop	