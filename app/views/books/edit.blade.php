@extends('layouts.form')

@section('content')


	<h1> editing </h1>


	{{ Form::model($books, ['method' => 'PATCH', 'route' =>[ 'books.update', $books->id]]) }}
       <div>
			{{ Form::label('title', 'Title') }}

			{{ Form::input('text','title', $books->title) }}

			{{$errors->first('title') }}
	</div>
	<br>
	<br>
  <div>
			{{ Form::label('coverurl', 'Book Cover url') }}

			{{ Form::input('text', 'coverurl') }}

			{{$errors->first('coverurl') }}
	</div>

	<div>
		{{ Form::label('content', 'Content') }}

		{{ Form::textarea('content', $value = null, array('class' => 'writeform')) }}

		{{ $errors->first('content') }}
	</div>
	<div>
	 

	 {{ Form::hidden('author', Auth::user()->username) }} 
 	{{ Form::hidden('published', $books->published) }} 
	 </div>
	<input type="submit" name="Post" value="Publish">
<input type="submit" name="draft" value="Save as Draft">

	{{ Form::close() }}


	@stop	