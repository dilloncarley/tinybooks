<?php

class SessionsController extends BaseController{

	public function create()

	{

		if (Auth::check()) return Redirect::to('/users');
		return View::make('sessions.login');

	}

	public function store()

	{

		if (Auth::attempt(Input::only('email', 'password' )))
		{

		
			return Redirect::to('users');

		}
		return 'Failed!';


	}

	public function destroy()

	{
		Session::flush();

		return Redirect::to('/login');





	}
}