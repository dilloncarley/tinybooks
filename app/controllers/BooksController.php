<?php

class BooksController extends \BaseController {
 	
	protected $book;

	public function __construct(Books $books){

		$this->books = $books;

		

	}

	public function index()
	{
		 $index = View::make('books.index');

		

			
   
		
	

		return $index;
	



	}

public function drafts()
	{
		 $usersdrafts = View::make('books.drafts');
		
		

			
		$drafts = DB::table('books')->where('published', '=', '1')->where('author', '=', Auth::user()->username)->get();

		

		return $usersdrafts->with('drafts', $drafts);
	



	}


 
	public function show($id)
	{


	$books =  $this->books->whereid($id)->where('published', '=', '0')->first();

	

	
    $strings = wordwrap($books->content, 350, "@"); //Put a {div} every 10 characters or on a @ sign 
    $chunks = explode("@", $strings);


//insert vote
$favorites = DB::table('favorites')->whereUserId(Auth::user()->id)->lists('books_id');

//add up all favorites for each book
   
	$counts = DB::select('select favorites.*, count(books_id) AS  count

from favorites
where favorites.books_id = '.$books->id.'
group by favorites.books_id');
    

	return View::make('books/show', ['books'=> $books])->with('chunks', $chunks)->with('favorites', $favorites)->with('counts', $counts);

	}




public function create()
	{

	$value = Auth::user()->username;
	return View::make('books.write')->with('value', $value);
	}





public function store()
	{

		

		$input = Input::all();
	
	if ( ! $this->books->isValid($input))

		{

			return Redirect::back()->withInput()->withErrors($this->books->errors);
		
		}

		if(isset($input['Post'])){
			$input = Input::all();

				  $input['published'] = 0;

			   
			      $this->books->create($input);


			      	
			      	
		return Redirect::to('/users/'.Auth::user()->username.'');

 
    	}

    if(isset($input['draft'])){

		       $input['published'] = 1;

		       $this->books->create($input);
		       

    return Redirect::to('/drafts');
    }

		return Redirect::route('users.index');

	}
public function destroy($usersbookid)
	{

		$destroy = Books::find($usersbookid);

$destroy->delete();

return Redirect::back();






    }

    public function booksSearch()
{
	$q = Input::get('query');

	$books = $this->books->whereRaw(
		"MATCH(title,author,coverurl) AGAINST(? IN BOOLEAN MODE)", 
		array($q)
	)->get();

	return View::make('books.index', compact('books'));

}

public function edit($bookid)
	{

	


	
	
	//Edit if the author of that book matches the one trying to edit the book
	$books =  $this->books->whereid($bookid)->where('author', '=', Auth::user()->username)->first();

	

	return View::make('books.edit', compact('books'));
	}






public function update($bookid)
	{
		
	if ( ! $this->books->isValid(Input::all()))

		{

			return Redirect::back()->withInput()->withErrors($this->books->errors);
		
		}
	
    $books = Books::findOrFail($bookid);

	$books->fill(Input::all());
	
	//checks to see if the a
		if (Input::has('Post'))
		{

		   $books['published'] = 0;

		   return Redirect::to('/users/'.Auth::user()->username.'');
		}

		else if (Input::has('draft'))
		{

		    $books['published'] = 1;

		    return Redirect::to('/drafts');
		}
	


	$books->save();
		

	return Redirect::to('books/'.$books->id.'/edit');	
	}






 
}