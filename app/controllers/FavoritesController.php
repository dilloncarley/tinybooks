<?php
class FavoritesController extends BaseController {  
    public function add() {
        return View::make( 'options/new' );
    }
    
    public function create() {
        //check if its our form
      if ( Session::token() !== Input::get( '_token' ) ) {
            return Response::json( array(
                'msg' => 'Unauthorized attempt to create option'
            ) );
        }

        $favorite = Input::get('books-id');
         Auth::user()->favorites()->attach($favorite);
        
      
        
        return Redirect::back();
    }
}