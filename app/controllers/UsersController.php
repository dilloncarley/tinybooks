<?php

class UsersController extends \BaseController {

	protected $user;

	public function __construct(User $user){

		$this->user = $user;

		
	}

	public function index()
	{
		$index = View::make('users.index');
		
 		
		
		$users = $this->user->all();
		

		return $index->with('users', $users);
	



	}

	public function show($username)
	{

	$user =  $this->user->whereUsername($username)->first();

	$usersbooks = DB::table('books')->where('author', $username)->orderBy('id', 'DESC')->get();

	return View::make('users/show', ['user'=> $user])->with('usersbooks', $usersbooks);
	}


	public function create()
	{

	return View::make('users.create');
	}

	public function store()
	{
		$input = Input::all();


		if ( ! $this->user->isValid($input))

		{

			return Redirect::back()->withInput()->withErrors($this->user->errors);
		
		}

		
		$input['password'] = Hash::make($input['password']);
		$input['password_confirmation'] = Hash::make($input['password_confirmation']);
		$this->user->create($input);

		
		
		

		
		return Redirect::route('users.index');
	}


}
